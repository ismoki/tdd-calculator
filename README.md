# TDD String calculator Kata example solution

The solution was built incrementally using TDD. To better understand how the solution evolved over the TDD process, view the "history" of the solution by checking out different steps along the way. 

To view different steps of the kata use version control to fetch tags which point to different steps 
- 'git fetch --tags'

To checkout the code on certain step use
- 'git checkout step<X>, where <X> is the number of the step (e.g., 'git checkout step1')

To view the changes between two steps you can use
- 'git diff step0 step1'

To return to the final solution use 'git checkout master'