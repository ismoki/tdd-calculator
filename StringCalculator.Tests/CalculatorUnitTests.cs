using System;
using NUnit.Framework;

namespace StringCalculator.Tests
{
    public class Tests
    {
        private Calculator calculator; 

        [SetUp]
        public void Setup()
        {
            this.calculator = new Calculator();
        }

        [Test]
        public void ShouldReturn0_WhenStringEmpty()
        {
            Assert.AreEqual(0, calculator.Add(""));
        }

        [TestCase("1", 1)]
        [TestCase("3", 3)]
        [TestCase("13412", 0)]
        public void ShouldReturnInput_WhenSingleNumberGiven(string input, int expected)
        {
            Assert.AreEqual(expected, calculator.Add(input));
        }

        [TestCase("0,0", 0)]
        [TestCase("0,1", 1)]
        [TestCase("2,3", 5)]
        [TestCase("1,1", 2)]
        [TestCase("1\n2", 3)]
        public void ShouldReturnSum_WhenTwoNumbersGiven(string input, int expected)
        {
            Assert.AreEqual(expected, calculator.Add(input));
        }

        [TestCase("0,0,0", 0)]
        [TestCase("1,1,1", 3)]
        [TestCase("2,3,5", 10)]
        [TestCase("1,2,3,4,5,6,7,8,9,10", 55)]
        [TestCase("1\n2,3", 6)]
        public void ShouldReturnSum_WhenMoreNumbersAreGiven(string input, int expected)
        {
            Assert.AreEqual(expected, calculator.Add(input));
        }


        public void ShouldUseCustomDelimiter()
        {
            Assert.AreEqual(6, calculator.Add("//[;]\n1;2;3"));
        }

        [Test]
        public void ShouldUseLongDelimiter()
        {
            Assert.AreEqual(6, calculator.Add("//[***]\n1***2***3"));
        }

        [TestCase("//[*][%]\n1*2%3", 6)]
        [TestCase("//[***][-%-]\n1***2-%-3", 6)]
        public void ShouldUseMultipleDelimiters(String input, int expected)
        {
            Assert.AreEqual(expected, calculator.Add(input));
        }

        [Test]
        public void negativeNumberShouldThrowException()
        {
            Assert.Throws<ArgumentException>(() => calculator.Add("1,-1"));
        }

        [Test]
        public void negativeNumbersShouldBeListedInException()
        {
            Exception ex = Assert.Throws<ArgumentException>(() => calculator.Add("1,-1,-4"));
            Assert.That(ex.Message, Does.Contain("-1"));
            Assert.That(ex.Message, Does.Contain("-4"));
        }

        [Test]
        public void numbersLargerThan1000ShouldBeIgnored()
        {
            Assert.AreEqual(2, calculator.Add("2,1001"));
        }

        [Test]
        [Ignore("Should cause error")]
        public void test()
        {
            Assert.AreEqual(1, "1,\n");
        }
    }
}