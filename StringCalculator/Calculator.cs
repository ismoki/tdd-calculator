﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringCalculator
{
    public class Calculator
    {
        private const string DelimiterPrefix = "//";
        private const string DefaultDelimiter = ",";
        readonly string[] LineSeparators = { Environment.NewLine, "\n" };

        public int Add(string numbers)
        {
            int sum = 0;
            string[] delimiters = { DefaultDelimiter };
            List<string> invalidValues = new List<string>();

            List<string> lines = numbers.Split(LineSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();

            if (lines.Count == 0) return 0;

            if (lines.First().StartsWith(DelimiterPrefix))
            {
                delimiters = parseDelimiters(lines.First());
                lines.Remove(lines.First());
            }

            foreach (string line in lines) { 

                foreach (string valueStr in line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries))
                {
                    try
                    {
                        sum += parseValue(valueStr);
                    }
                    catch (ArgumentException)
                    {
                        invalidValues.Add(line);
                    }
                }
            }

            if (invalidValues.Count > 0) 
                throw new ArgumentException("Negatives not allowed [ " + String.Join(", ", invalidValues) + "]");
            
            return sum;
        }

        private static string[] parseDelimiters(string delimiterLine)
        {
            const string DelimiterPattern = @"(?<=\[)[^\].]{0,}(?=\])"; // Regex pattern that matches characters between '[' and ']'

            return Regex.Matches(delimiterLine, DelimiterPattern)
                .Cast<Match>()
                .Select(m => m.Value)
                .ToArray();
        }

        private static int parseValue(string str)
        {
            int value = int.Parse(str);
            if (value < 0) throw new ArgumentException("Negatives not allowed");
            if (value > 1000) return 0;
            return value; 
        }
    }
}
